The component is representing a badge, marking the experimental features.
It is supposed to be used with the AI experiments, and comes with a popover explaining
what experiment means.

## Usage

```html
<gl-experiment-badge experiment-help-page-url="https://gitlab.com" popover-placement="bottom" />
```
