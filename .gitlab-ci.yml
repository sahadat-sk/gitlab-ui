stages:
  - pre-build
  - test
  - build
  - deploy
  - publish
  - manual

default:
  interruptible: true
  artifacts:
    expire_in: 30 days
  tags:
    - gitlab-org

variables:
  PUPPETEER_VERSION: '15.5.0'
  PUPPETEER_IMAGE: $CI_REGISTRY_IMAGE/puppeteer-v2:$PUPPETEER_VERSION
  CYPRESS_CACHE_FOLDER: $CI_PROJECT_DIR/.cypress_cache/Cypress

include:
  - template: Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.latest.gitlab-ci.yml
  - template: Jobs/Container-Scanning.latest.gitlab-ci.yml
  - template: Jobs/SAST.latest.gitlab-ci.yml
  - template: Jobs/Secret-Detection.latest.gitlab-ci.yml
  - project: gitlab-org/frontend/frontend-build-images
    file: /semantic-release/.gitlab-ci-template.rules.yml
  - project: gitlab-org/frontend/untamper-my-lockfile
    file: 'templates/merge_request_pipelines.yml'
  - project: 'gitlab-org/quality/pipeline-common'
    file: '/ci/danger-review.yml'

.is-merge-train: &is-merge-train
  if: '$CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"'

.is-merged-result: &is-merged-result
  if: '$CI_MERGE_REQUEST_EVENT_TYPE == "merged_result"'

code_quality:
  needs: []
  # Code quality scanning doesn't work otherwise on MR pipelines
  rules:
    - when: always
  # It's running docker-in-docker, so we want to set the correct tag
  tags:
    - gitlab-org-docker

container_scanning:
  variables:
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/puppeteer
    CI_APPLICATION_TAG: $PUPPETEER_VERSION
  needs: ['build_docker_image']

# Execute the security scanners immediately
gemnasium-dependency_scanning:
  needs: []

semgrep-sast:
  needs: []

nodejs-scan-sast:
  needs: []

secret_detection:
  needs: []

.puppeteer:
  image: $PUPPETEER_IMAGE
  needs: ['build_docker_image']

.node:
  image: node:20.9.0-buster
  variables:
    PUPPETEER_SKIP_DOWNLOAD: 'true'


.use_vue3:
  variables:
    VUE_VERSION: '3'

# This is a cache template for caching node_modules
# As a cache key we are using a SHA of .gitlab-ci.yml and yarn.lock
# The latter is obvious, because it updates when we update dependencies
# The former is to invalidate caches, in case we touch our CI config, which
# could mean changing something in our caching logic
.cache-template: &cache-template
  paths:
    - node_modules/
    - .cypress_cache/Cypress
  key:
    files:
      - .gitlab-ci.yml
      - yarn.lock
    prefix: node_modules

.yarn_install:
  before_script:
    - apt-get install -y make
    - yarn install --frozen-lockfile
  cache:
    <<: *cache-template
    policy: pull

# Only start pipelines on Merge Requests or the default branch
workflow:
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: always
    - if: $CI_MERGE_REQUEST_IID
      when: always
    - when: never

# Only run on GitLab UI default branches
.rules:gitlab-ui-default-branch:
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH && $CI_PROJECT_PATH == "gitlab-org/gitlab-ui"'

# Only run on merge requests that come from GitLab UI or
# from forks when a GitLab UI team member triggered a pipeline
.if-gitlab-ui-mr: '$CI_MERGE_REQUEST_IID && $CI_PROJECT_PATH == "gitlab-org/gitlab-ui"'

.rules:gitlab-ui-mr:
  rules:
    - if: !reference [.if-gitlab-ui-mr]

.rules:gitlab-ui-mr-manual:
  rules:
    - if: !reference [.if-gitlab-ui-mr]
      when: manual
      allow_failure: true

build_docker_image:
  variables:
    DOCKER_HOST: tcp://docker:2375/
  tags:
    - gitlab-org-docker
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  stage: pre-build
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - ./bin/build-docker.sh

# As we are caching based on the contents of our CI config and dependency lock file
# We only need to execute when these change. However, we give a manual job as an escape hatch
populate_npm_cache:
  extends: [.node, .yarn_install]
  stage: pre-build
  script:
    - echo "successfully installed dependencies"
  cache:
    <<: *cache-template
    policy: push
  rules:
    - changes:
        - .gitlab-ci.yml
        - yarn.lock
      when: always
    - when: never

build_package:
  extends: [.node, .yarn_install]
  variables:
    TAR_ARCHIVE_NAME: gitlab-ui.$CI_COMMIT_REF_SLUG.tgz
  needs: []
  stage: build
  script:
    - yarn build
    - yarn pack --filename $TAR_ARCHIVE_NAME
    - DEPENDENCY_URL="$CI_PROJECT_URL/-/jobs/$CI_JOB_ID/artifacts/raw/$TAR_ARCHIVE_NAME"
    - echo "The package.json dependency URL is $DEPENDENCY_URL"
    - echo "DEPENDENCY_URL=$DEPENDENCY_URL" > build_package.env
  artifacts:
    when: always
    reports:
      dotenv: build_package.env
    paths:
      - dist
      - src/scss/utilities.scss
      - $TAR_ARCHIVE_NAME

.build_storybook:
  extends: [.node, .yarn_install]
  needs: []
  stage: build
  script:
    - bin/build-storybook-static.sh
  artifacts:
    paths:
      - public

build_storybook:
  extends: [.build_storybook]

build_storybook_vue3:
  extends: [.build_storybook, .use_vue3]

lint:
  extends: [.node, .yarn_install]
  needs: []
  stage: test
  script:
    - yarn eslint
    - yarn prettier
    - yarn stylelint
    - yarn markdownlint
    - >
      grep -r -i '<style' --include \*.vue components
      && echo "Vue components should not contain <style tags"
      && exit 1
      || echo "No Vue component contains <style tags"

generate_utility_classes:
  extends: [.node, '.yarn_install']
  needs: []
  stage: test
  script:
    - yarn generate-utilities

check_translations_dictionary:
  extends: [.node, '.yarn_install']
  needs: []
  script:
    - ./bin/check_translations_dict.sh

.visual:
  extends:
    - .puppeteer
    - .yarn_install
  needs:
    - build_docker_image
  stage: test
  script:
    - yarn test:visual
  rules:
    - !reference ['.rules:gitlab-ui-default-branch', rules]
    - <<: *is-merge-train
      when: always
    - if: '$CI_MERGE_REQUEST_IID'
      when: manual
      allow_failure: true
  artifacts:
    when: always
    paths:
      - tests/__image_snapshots__/

visual:
  extends: [.visual]

visual_vue3:
  extends: [.visual, .use_vue3]
  allow_failure: true

visual_minimal:
  extends: [.visual]
  script:
    - yarn test:visual:minimal
  rules:
    - <<: *is-merged-result
      when: always

.integration_tests:
  image: cypress/browsers:node16.18.0-chrome107-ff106-edge
  extends: [.node, .yarn_install]
  needs: []
  stage: test
  script:
    - yarn test:integration
  artifacts:
    when: on_failure
    expire_in: 1 week
    paths:
      - cypress

integration_tests:
  extends: [.integration_tests]

integration_tests_vue3:
  extends: [.integration_tests, .use_vue3]

.unit_tests:
  extends: [.node, .yarn_install]
  needs: []
  stage: test
  script:
    - yarn test:unit

unit_tests_vue2:
  extends: [.unit_tests]

unit_tests_vue3:
  extends: [.unit_tests, .use_vue3]

update_screenshots:
  extends:
    - .puppeteer
    - .yarn_install
    - .rules:gitlab-ui-mr-manual
  stage: manual
  script:
    - yarn test:visual:update
    - ./bin/update-screenshots.sh

review:
  extends:
    - .rules:gitlab-ui-mr
  stage: deploy
  needs:
    - build_storybook
  script:
    - rsync -av --delete public /srv/nginx/pages/$CI_COMMIT_REF_SLUG
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: http://$CI_COMMIT_REF_SLUG.$APPS_DOMAIN
    on_stop: review_stop
  tags:
    - nginx
    - review-apps
    - deploy

review_stop:
  extends:
    - .rules:gitlab-ui-mr-manual
  stage: manual
  needs:
    - review
  script:
    - rm -rf public /srv/nginx/pages/$CI_COMMIT_REF_SLUG
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  tags:
    - nginx
    - review-apps
    - deploy

pages:
  extends:
    - .rules:gitlab-ui-default-branch
  stage: deploy
  needs:
    - build_storybook
    - build_storybook_vue3
  script:
    - echo "Deploying to Pages"
  artifacts:
    paths:
      - public

create_integration_branch:
  extends:
    - .node
    - .rules:gitlab-ui-mr-manual
  stage: manual
  needs:
    - build_package
  script:
    - INTEGRATION_BRANCH="gitlab-ui-integration-$CI_COMMIT_REF_NAME"
    - git config --global user.email "$GITLAB_INTEGRATION_USER@noreply.gitlab.com"
    - git config --global user.name "GitLab UI Integration Test"
    - git clone https://gitlab.com/gitlab-org/gitlab.git gitlab --depth=1
    - cd gitlab
    - (git remote set-branches origin '*' && git fetch origin $INTEGRATION_BRANCH && git checkout $INTEGRATION_BRANCH) || git checkout -b $INTEGRATION_BRANCH
    - yarn add @gitlab/ui@$DEPENDENCY_URL
    - git add package.json yarn.lock
    - 'git commit -m "GitLab UI integration branch for $CI_COMMIT_REF_NAME"'
    - git push -u https://$GITLAB_INTEGRATION_USER:$GITLAB_INTEGRATION_TOKEN@gitlab.com/gitlab-org/gitlab.git HEAD

publish_to_npm:
  extends:
    - .semantic-release
    - .rules:gitlab-ui-default-branch
  # We need to run `publish` after pages, so that pages will get deployed
  # properly, as the publish-to-npm step will create a new commit and this
  # could lead to a side-effect where pages don't get published because of
  # the commit being made before `pages` had a chance to run
  stage: publish
  # This job doesn't use the DAG feature, because we don't want it to
  # run in case another job in the previous stages fails
  dependencies:
    - build_package
